
self.addEventListener('install', function(event) {
    console.log('[Service Worker] Installing Service Worker ...', event);
    event.waitUntil(
        caches.open('static')
        .then(function(cache) {
            console.log('[Service Worker] Precaching App Shell');
            cache.addAll([
                '/',
                '/icon/icon.png',
                '/images/icon.png',
                '/icon,icon512x512.png',
                '/Manifest/manifest.json',
                '/enquiry.html',
                '/dashboard.html',
                '/map/css/bootstap.min.css',
                'map/css/styles.css',
                'map/js/bootstap.min.js',
                '/map/js/jquery-2.1.3.min',
                '/map/index1.html',
                '/index.html',
                '/customerDetails.html',
                '/paymentmode.html',
                '/vehicleExchange.html',
                'vehicleExchange.html'
            ]);
        })
    )
});

self.addEventListener('activate', function(event) {
    console.log('[Service Worker] Activating Service Worker ....', event);
    return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
        .then(function(response) {
            if (response) {
                return response;
            } else {
                return fetch(event.request)
                    .then(function(res) {
                    return caches.open('dynamic')
                        .then(function(cache) {
                        cache.put(event.request.url, res.clone());
                        return res;
                    })
                });
            }
        })
    );
});
let deferredPrompt;
//window.addEventListener('beforeinstallprompt', (e) => {
//    // Prevent Chrome 67 and earlier from automatically showing the prompt
//    e.preventDefault();
//    // Stash the event so it can be triggered later.
//    deferredPrompt = e;
//});
//window.addEventListener('beforeinstallprompt', (e) => {
//    e.preventDefault();
//    deferredPrompt = e;
//    // Update UI notify the user they can add to home screen
//    btnAdd.style.display = 'block';
//});
//btnAdd.addEventListener('click', (e) => {
//    // hide our user interface that shows our A2HS button
//    btnAdd.style.display = 'none';
//    // Show the prompt
//    deferredPrompt.prompt();
//    // Wait for the user to respond to the prompt
//    deferredPrompt.userChoice
//        .then((choiceResult) => {
//        if (choiceResult.outcome === 'accepted') {
//            console.log('User accepted the A2HS prompt');
//        } else {
//            console.log('User dismissed the A2HS prompt');
//        }
//        deferredPrompt = null;
//    });
//});
//window.addEventListener('appinstalled', (evt) => {
//    app.logEvent('a2hs', 'installed');
//});
//
